from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from django.conf.urls.static import static


api_v1_patterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('apps.users.urls')),
    path('', include('apps.posts.urls')),
]

urlpatterns = [
    path("api/v1/", include(api_v1_patterns))
]

if settings.DEBUG:
    from drf_yasg import openapi
    from drf_yasg.views import get_schema_view
    from rest_framework import permissions

    schema_view = get_schema_view(
        openapi.Info(
            title="INSTAGRAM API",
            default_version='v1',
            description="INSTA API",
            terms_of_service="https://www.google.com/policies/terms/",
            contact=openapi.Contact(email="ruslanyakupoff996@gmail.com"),
            license=openapi.License(name="BSD License"),
        ),
        public=True,
        permission_classes=[permissions.AllowAny],
    )

    urlpatterns += [
        path('api/v1/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
        path('api/v1/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]



urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

