from django.contrib.auth import get_user_model

from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status
from rest_framework.decorators import action


from apps.users.serializers import UserSerializer, ProfileSerializer
from apps.users.models import Profile

User = get_user_model()


class UserAPIViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(
        detail=False,
        methods=["get"],
        permission_classes=[permissions.IsAuthenticated],
        serializer_class=UserSerializer
    )
    def get_userinfo(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProfileAPIViewset(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer