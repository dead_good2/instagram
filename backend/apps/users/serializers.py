from rest_framework import serializers

from apps.users.models import User, Profile
from apps.posts.serializers import PostSerializer

class UserSerializer(serializers.ModelSerializer):
    user_post = PostSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "created_at",
            "description",
            "user_post",
            "email",
            "password",
        )

    def update(self, instance, validated_data):
        password = validated_data.pop("password")
        user = super().update(instance, validated_data)
        user.set_password(password)
        user.save()
        return user

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = super().create(validated_data)
        user.set_password(password)
        user.is_active = True
        user.save()
        return user


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            "id",
            "user",
            "bio",
            "avatar",
            "followers",
            "following",
        )
        