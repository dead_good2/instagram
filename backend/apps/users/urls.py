from django.urls import path

from apps.users.views import UserAPIViewset, ProfileAPIViewset

from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)


router = DefaultRouter()

router.register(
    prefix="users",
    viewset=UserAPIViewset
)
router.register(
    prefix="profiles",
    viewset=ProfileAPIViewset
)

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]

urlpatterns += router.urls
