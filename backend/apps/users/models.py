from django.db import models

from django.db import models
from django.contrib.auth.models import AbstractUser

from apps.users.managers import CustomUserManager


class User(AbstractUser):
    username = models.CharField(
        max_length=20,
        verbose_name = "Имя пользователя",
        unique=True
    )
    email = models.EmailField(
        max_length=254,
        verbose_name="Почта",
        unique=True
    )
    password = models.CharField(
        verbose_name="Пароль",
        max_length=254,
    )
    created_at = models.DateField(
        verbose_name="Дата создания аккаунта",
        auto_now_add=True
    )
    description = models.TextField(
        verbose_name="Описание",
        blank=True, null=True
    )
    objects = CustomUserManager()

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def str(self) -> str:
        return self.username
    

class Profile(models.Model):
    user = models.OneToOneField(
        User, 
        on_delete=models.CASCADE,
        related_name='profile'
    )
    bio = models.TextField(
        max_length=500,
        verbose_name="Биография",
        blank=True
    )
    avatar = models.ImageField(
        upload_to='media/avatars/',
        verbose_name="Аватарка",
        blank=True
    )
    followers = models.ManyToManyField(
        User,
        related_name='followers',
        blank=True,
        verbose_name="Подписчики"
    )
    following = models.ManyToManyField(
        User,
        related_name='following',
        blank=True,
        verbose_name="Подписки"
    )

    class Meta:
        verbose_name='Профиль'
        verbose_name_plural='Профили'

    def str(self):
        return f'{self.user.username} Profile'
