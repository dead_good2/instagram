from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from apps.posts.models import Post, Comment, Like
from apps.posts import serializers


class PostAPIViewset(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    # def get_permission_classes(self):
    #     if self.action == "create":
    #         return [IsAuthenticated()]
    #     return self.permission_classes

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CommentAPIViewset(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = serializers.CommentSerializer

    def perfome_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_serializer_class(self):
        if self.action in ("create", "update"):
            return serializers.CommentSerializer
        if self.action == 'retrieve':
            return serializers.CommentDetailSerializer
        return self.serializer_class


class LikeAPIViewset(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = serializers.LikeSerializer
