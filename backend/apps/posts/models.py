from django.db import models

from django.contrib.auth import get_user_model


User = get_user_model()

class Post(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='Владелец поста',
        related_name='user_post'
    )
    image = models.ImageField(
        upload_to='media/posts/',
        verbose_name='Фото'
    )
    caption = models.TextField(
        max_length=1000,
        verbose_name='Подпись'
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Дата создания",
        blank=True, null=True
    )   

    class Meta:
        verbose_name='Пост'
        verbose_name_plural='Посты'

    def str(self):
        return f'Post by {self.user.username}'
    

class Comment(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='Комментарии юзера',
        related_name='user_comment'
    )
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        verbose_name='Комментарий к посту',
        related_name='post_comments'
    )
    text = models.TextField(
        max_length=1000,
        verbose_name='Текст комментария'
    )
    sub_comment = models.ForeignKey(
        'self',
        related_name = "comm_to_comm",
        verbose_name = "Дочерние комментарии",
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )

    class Meta:
        verbose_name='Комментарий'
        verbose_name_plural='Комментарии'
    
    def str(self) -> str:
        return f'Comment by {self.user.username}'


class Like(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='like_by_user'
    )
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='like_post'
    )

    class Meta:
        verbose_name='Лайк'
        verbose_name_plural='Лайки'

    def __str__(self):
        return f'Like by {self.user.username}'
    