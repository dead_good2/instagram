from rest_framework.serializers import ModelSerializer, SerializerMethodField

from apps.posts.models import Post, Comment, Like

class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = (
            "id",
            "user",
            "post",
            "text",
            "created_at",
        )


class PostSerializer(ModelSerializer):
    post_comments = CommentSerializer(read_only=True,many=True)

    class Meta:
        model = Post
        fields = (
            "id",
            "user",
            "image",
            "caption",
            "created_at",
            "post_comments",
        )
        read_only_fields = (
            "id",
            "user",
            "created_at"
        )

    # def get_profile(self, obj):
    #     return ProfileSerializer(obj.user.profile).data
class CommentDetailSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = (
            "id",
            "sub_comment",
        )


class LikeSerializer(ModelSerializer):
    class Meta:
        model = Like
        fields = (
            "id",
            "user",
            "post"
        )
