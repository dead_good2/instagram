from rest_framework.routers import DefaultRouter

from apps.posts.views import PostAPIViewset, CommentAPIViewset, LikeAPIViewset


router = DefaultRouter()
router.register(
    prefix="posts",
    viewset=PostAPIViewset
)
router.register(
    prefix="comments",
    viewset=CommentAPIViewset
)
router.register(
    prefix="likes",
    viewset=LikeAPIViewset
)



urlpatterns = router.urls