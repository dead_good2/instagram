from django.contrib import admin

from apps.posts.models import Comment

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "user",
        "post",
        "text",
        "sub_comment"
    ]
    

